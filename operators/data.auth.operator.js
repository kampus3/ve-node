// Imports
const bcrypt = require('bcryptjs'); //=> https://www.npmjs.com/package/bcryptjs
const jwt = require('jsonwebtoken'); //=> https://www.npmjs.com/package/jsonwebtoken
const CryptoJS = require("crypto-js"); //=> https://www.npmjs.com/package/crypto-js
const { decryptObjectValue } = require("../ve-openapi/services/content.service");
const { fetchCrudRequest } = require('../ve-openapi/services/fetch.service');

/**
 * Register new identity from client request
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const authRegisterOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            // Hash password
            request.sqlObject.password = await bcrypt.hash( request.sqlObject.password, 10 );

            // Send AJAX request to 'identity.api'
            const identityResponse = await fetchCrudRequest( 
                'post',
                `${ process.env.NODE_IDENTITY_API_URL }/v1/auth/register`,
                'http://localhost:8977',
                null,
                null,
                {
                    email: request.sqlObject.email,
                    password: request.sqlObject.password,
                }
            )

            // Delete unused properties
            delete request.sqlObject.email;
            delete request.sqlObject.password;

            // Add 'enduser' author
            request.sqlObject.author = identityResponse.data.uuid;

            // Save new 'enduser' informations
            sqlClient.query(`INSERT INTO enduser SET ?`, request.sqlObject,  (error, results, fields) => {
                if (error) throw error;
                else{ 
                    return resolve( {identity: identityResponse.data, enduser: request.sqlObject} ) 
                }
            });
        } 
        catch (error) { return reject(error) }
    })
}

/**
 * Log identity from email and password
 * TODO: login with 'identity.api'
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const authLoginOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            // Send AJAX request to 'identity.api'
            const identityResponse = await fetchCrudRequest( 
                'post',
                `${ process.env.NODE_IDENTITY_API_URL }/v1/auth/login`,
                'http://localhost:8977',
                null,
                null,
                {
                    email: request.sqlObject.email,
                    password: request.sqlObject.password,
                }
            )

            // Get 'enduser' value from token
            sqlClient.query(`SELECT * FROM enduser WHERE author = '${ identityResponse.data.uuid }'`, async (error, results) => {
                if (error) throw error;
                else{
                    if( !results[0] ){ throw `No 'enduser' informations` }
                    else{
                        // Uncrypt 'enduser' informations
                        const enduser = await decryptObjectValue(results[0], ['name', 'age'])

                        // Return 'enduser' informations and JWT token
                        return resolve({
                            enduser: Object.assign( enduser, { email: request.sqlObject.email } ), 
                            token: identityResponse.data.token
                        })
                    }
                }
            });
        } 
        catch (error) { return reject(error) }
    })
}

/**
 * Extract identity token and associated informations
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const authTokenOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            // Get identity associated informations
            sqlClient.query(`SELECT * FROM article WHERE author = '${ request.authIdentity.uuid }'`, async (error, results) => {
                if (error){ throw `Article not found` }
                else{
                    // Decrypt object
                    for( let item of results ){
                        item = await decryptObjectValue( item, ['title', 'content'] )
                    }
                    
                    // Resolve decoded JWT object from 'content.service' and identity associated informations
                    return resolve( { identity: request.authIdentity, articles: results } )
                }
            })
        } 
        catch (error) { return reject(error) }
    })
}

// Export shared service functions
module.exports = {
    authRegisterOperator,
    authLoginOperator,
    authTokenOperator
}