// Import
const { decryptObjectValue } = require("../ve-openapi/services/content.service");

/**
 * Generate new article from client reqest
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const articleCreateOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            // Add author UUID
            request.sqlObject.author = request.authIdentity.uuid;

            // Send SQL query
            sqlClient.query(`INSERT INTO article SET ?`, request.sqlObject,  (error, results, fields) => {
                if (error) throw error;
                else{ return resolve( { results, fields } ) }
            });
        } 
        catch (error) { return reject(error) }
    })
}

/**
 * Get all article from database
 * @param {object} sqlClient - MYSQL client object
 * @param {object} request - Node request object
 */
const articleGetOperator = ( sqlClient, request ) => {
    return new Promise( async (resolve, reject) => {
        try {
            sqlClient.query(`SELECT * FROM article`, async (error, results) => {
                if (error){ throw `Article not found` }
                else{
                    // Decrypt object
                    if( request.pathDescription.requestBody.crypted ){
                        for( let item of results ){
                            item = await decryptObjectValue(item, request.pathDescription.requestBody.crypted)
                        }
                    }
                    
                    // Retun result
                    return resolve(results) 
                }
            })
        } 
        catch (error) { return reject(error) }
    })
}

module.exports = {
    articleCreateOperator,
    articleGetOperator,
}