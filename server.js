/* 
Imports
*/
    // Import NPM modules
    require('dotenv').config(); //=> https://www.npmjs.com/package/dotenv
    const express = require('express'); //=> https://www.npmjs.com/package/express
    const path = require('path');

    // Import application modules
    const apiDocumentation = require('./api.description');
    const { articleCreateOperator, articleGetOperator } = require('./operators/data.article.operator.js.js');
    const { authRegisterOperator, authLoginOperator, authTokenOperator } = require('./operators/data.auth.operator');
    const { mapRequestBody, extractJwt } = require('./ve-openapi/services/content.service');
    const { mysqlObject } = require('./ve-openapi/services/mysql.service');
    
//

/* 
Server class
*/
    class ServerClass{
        constructor(){
            // Set static application value
            this.server = express();
            this.port = process.env.NODE_API_PORT;
            this.version = process.env.NODE_API_VERSION;
            this.mysql = mysqlObject;

            // Add API operations (defined in api.description.js)
            this.operations = {
                authRegisterOperator,
                authLoginOperator,
                authTokenOperator,
                articleCreateOperator,
                articleGetOperator,
            }
        }

        init(){
            // Define static folder
            this.server.set( 'views', __dirname + '/www' );
            this.server.use( express.static(path.join(__dirname, 'www')) );

            //=> Set body request with ExpressJS (http://expressjs.com/fr/api.html#express.json)
            this.server.use(express.json({limit: '20mb'}));
            this.server.use(express.urlencoded({ extended: true }))

            // Connect MYSQL database
            this.mysql.connect();

            // Define server middleware
            this.server.use( async ( request, response, next ) => {
                // Update request object
                request.activeMethod = request.method.toLowerCase();
                request.activePath = request.path;
                request.pathDescription = apiDocumentation.paths[request.activePath][request.activeMethod];
                
                // Server security
                try {
                    // Check security
                    if( request.pathDescription.security.indexOf('Bearer') !== -1 ){
                        // Extract JWT access token from headers
                        const authHeader = request.headers['authorization'];
                        const headerToken = authHeader && authHeader.split(' ')[1];
    
                        // Decode client access token
                        const { isAuth, decoded } = await extractJwt( process.env.NODE_API_NAME, this.mysql, headerToken );
    
                        // Check access token
                        if( !isAuth || decoded === null ){ throw { status: 401, message: `User unauthorized`, content: null } }
                        else{
                            // Add decoded JWT user token in the request object
                            request.authIdentity = decoded;
                        }
                    }
    
                    // Check if client body is needed
                    if( request.pathDescription.requestBody.required ){
                        // Map client request body
                        const { ok, miss, extra, content, sqlObject } = mapRequestBody( 
                            request.body, request.pathDescription.requestBody, request.pathDescription.model, request.activeMethod, null 
                        )
        
                        // Check client request
                        if( !ok ){
                            throw { status: 400, message: `Request client is not valid`, content: { miss, extra } }
                        }
                        else{
                            // Add SQL body in request
                            request.sqlObject = content;
                            return next()
                        }
                    }
                    else{
                        // Enable access
                        return next()
                    }
                } 
                catch ( error ) {
                    return response.status( error.status ).json({
                        endpoint: request.activePath,
                        method: request.activeMethod,
                        mesage: error.message,
                        data: null,
                        error: error.content
                    })
                }
            })

            // Start router
            this.setup();
        }

        setup(){
            // [OPENAPI] Get each API path
            for( let path in apiDocumentation.paths ){
                // [OPENAPI] Get each path method path
                for( let method in apiDocumentation.paths[path] ){
                    this.server[ method ]( path, ( request, response ) => {
                        // Use server operations
                        this.operations[ apiDocumentation.paths[path][method].operationId ]( this.mysql, request  )
                        .then( data => {
                            return response.status(200).json({
                                endpoint: path,
                                method: request.method,
                                mesage: `Success request`,
                                data: data,
                                error: null
                            })
                        })
                        .catch( error => {
                            return response.status(404).json({
                                endpoint: path,
                                method: request.method,
                                mesage: `Error request`,
                                data: null,
                                error: error
                            })
                        })
                    })
                }
            }

            // Start server
            this.launch();
        }

        launch(){
            // Start server
            this.server.listen( this.port, () => {
                console.log({
                    node: `http://localhost:${ this.port }`,
                    mysql: this.mysql.config
                })
            })
        }
    }
//

/* 
Start server
*/
    const nodeApi = new ServerClass();
    nodeApi.init();
//